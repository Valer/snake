import operator
from functools import partial
from typing import Type


class Vector(tuple):

    def __new__(cls: Type['Vector'], *values):
        assert all(map(lambda x: isinstance(x, int), values))
        return super(Vector, cls).__new__(cls, values)

    def __add__(self, other):
        if not isinstance(other, Vector):
            return NotImplemented
        assert len(self) == len(other)
        return Vector(*map(operator.add, self, other))

    def __mul__(self, other):
        if not isinstance(other, int):
            return NotImplemented
        return Vector(*map(partial(operator.mul, other), self))
