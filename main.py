import logics
from kivy.app import App
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.graphics import Color, Rectangle
from kivy.uix.widget import Widget


class SnakeWidget(Widget):
    def __init__(self):
        super(SnakeWidget, self).__init__()
        self.keyboard = Window.request_keyboard(self.keyboard_closed, self)
        self.keyboard.bind(on_key_down=self.on_keyboard_down)
        self.bind(width=self.draw_game, height=self.draw_game)
        self.mechanics = logics.SnakeGame()
        self.position = None
        self.cell_size = None
        self.offset = None

    def draw_game(self, *args):
        widget_size = min(self.width, self.height)
        common_offset = (max(self.width, self.height) - widget_size) / 2
        if self.width > self.height:
            self.offset = [common_offset, 0]
        else:
            self.offset = [0, common_offset]

        self.cell_size = widget_size / self.mechanics.field_size
        all_snake = []
        for one_snake_cell in self.mechanics.snake_position:
            cell_position = [coord * self.cell_size + offset for
                             coord, offset in
                             zip(one_snake_cell, self.offset)]
            all_snake.append(cell_position)
        with self.canvas:
            self.canvas.clear()
            Color(1, 1, 1)
            Rectangle(size=(widget_size, widget_size), pos=(self.center_x - widget_size / 2,
                                                            self.center_y - widget_size / 2))
            self.draw_snake(all_snake)
            self.draw_food()

    def draw_snake(self, snake_coords):
        for x in snake_coords:
            Color(1, 0, 0)
            Rectangle(size=(self.cell_size, self.cell_size), pos=x)

    def draw_food(self):
        position = [coord * self.cell_size + offset for
                    coord, offset in zip(self.mechanics.food, self.offset)]
        Color(0, 0, 1)
        Rectangle(size=(self.cell_size, self.cell_size), pos=position)

    def update_game(self, dt, *args):
        self.mechanics.next_step()
        self.draw_game()

    def on_keyboard_down(self, keyboard, keycode, text, modifiers):
        key_name = keycode[1]
        if key_name == 'up':
            self.mechanics.turn((0, 1))
        elif key_name == 'down':
            self.mechanics.turn((0, -1))
        elif key_name == 'right':
            self.mechanics.turn((1, 0))
        elif key_name == 'left':
            self.mechanics.turn((-1, 0))

    def keyboard_closed(self):
        self.keyboard.unbind(on_key_down=self._on_keyboard_down)
        self.keyboard = None


class SnakeApp(App):

    def build(self):
        game = SnakeWidget()
        Clock.schedule_interval(game.update_game, 50 / 60.0)
        return game


if __name__ == '__main__':
    SnakeApp().run()
