from collections import deque
from random import randint
from typing import Tuple

from vector_math import Vector


class SnakeGame:

    def __init__(self, field_size: int = 10) -> None:
        self.field_size = field_size
        self.snake_position = deque()
        self.snake_position.append(Vector(int(field_size / 2), int(field_size / 2)))
        self.all_snake = set(self.snake_position)
        self.direction = Vector(1, 0)
        self.new_direction = Vector(1, 0)
        self.is_alive = True
        self.food = self.serve_new_food()

    def next_step(self):
        if self.direction != self.new_direction:
            self.direction = self.new_direction
        which_cell_next = self.direction + self.snake_position[-1]
        if not (
            0 <= which_cell_next[0] <= (self.field_size - 1) and 0 <= which_cell_next[1] <= (self.field_size - 1)) or \
            which_cell_next in self.all_snake:
            self.is_alive = False
            return
        self.snake_position.append(which_cell_next)
        self.all_snake.add(which_cell_next)
        if which_cell_next == self.food:
            self.food = self.serve_new_food()
        else:
            deleted_item = self.snake_position.popleft()
            self.all_snake.discard(deleted_item)

    allowed_directions = {Vector(1, 0), Vector(0, 1), Vector(-1, 0), Vector(0, -1)}

    def turn(self, wanted_direction: Tuple[int, int]):
        processed_wanted_direction = Vector(wanted_direction[0], wanted_direction[1])
        assert processed_wanted_direction in self.allowed_directions
        result_vector = self.direction + processed_wanted_direction
        if result_vector != Vector(0, 0):
            self.new_direction = processed_wanted_direction

    def serve_new_food(self):
        food_pos = None
        while food_pos is None:
            new_position = Vector(randint(0, (self.field_size - 1)), randint(0, (self.field_size - 1)))
            if new_position not in self.snake_position:
                food_pos = new_position
        return food_pos
